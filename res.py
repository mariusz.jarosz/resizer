
'''
Program grupowo zmieniający rozmiar plików graficznych
'''
from tkinter.ttk import  Frame, Style, Label, Entry, Button
from tkinter import *
from tkinter import filedialog
from PIL import Image

l = []

def otworz_pliki():
    global l # odniesienie do globalnej zmiennej
    #  otwarcie pliku
    filename =  filedialog.askopenfilenames(initialdir = "/",title = "Wybierz pliki",filetypes = (("JPG","*.jpg"),("Bitmapa","*.bmp"), ("TIFF","*.tiff"), ("JPEG","*.jpeg"), ("wszyskie pliki","*.*")))
    #  dodanie ścieżki do plików do globalnej listy
    for i in filename:
        l.append(i)
    # wpiosanie ścieżki do wyświetlanej listy
    list1.delete(0,END)
    for row in l:
        list1.insert(END,row)
    return l

def zapisz_pliki():
    mnoznik = float(s.get())/100
    for sciezka in l:
        im=Image.open(sciezka)
        w,h = im.size
        size=int(w*mnoznik), int(h*mnoznik)
        im=im.resize(size)
        nowa_sciezka= sciezka[:sciezka.rfind('/')+1] + "resized_" + sciezka[sciezka.rfind('/')+1:]
        im.save(nowa_sciezka)

window = Tk()
window.wm_title("Resizer")

styl=Style()
styl.theme_use('winnative')

# przyciski
b1 = Button(window, text="Otwórz", width=12, command=otworz_pliki)
b1.grid(row=6, column=0)
b2 = Button(window, text="Zapisz", width=12, command=zapisz_pliki)
b2.grid(row=6, column=3)

# etykieta
var = StringVar()
e = Label(window, text= "Procent:")
e.grid(row=6, column=1)

s=Entry(window, width=12)
s.grid(row=6, column=2, sticky="W")

# lista
list1 = Listbox(window, height=6, width=87)
list1.grid(row=0, column=0, rowspan=6, columnspan=3, padx=5, pady=4)

# scrollbar
sb1 = Scrollbar()
sb1.grid(row=0, column=3, rowspan=6)

# połączenie scrollbar z listą
list1.configure(yscrollcommand=sb1.set)
sb1.configure(command=list1.yview)

window.mainloop()
