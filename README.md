Prosty program/skrypt do masowej zmiany rozmiaru zdjęć.</br>

Plik .exe znajduje się w folderze dist</br>

Po uruchomieniu programu należy za pomocą przycisku "Otwórz" wskazać pliku, których chcemy zmienić rozmiar. Następnie w oknie "Procent" podać o jaki procent chcemy zmniejszyć/zwiększyć pliki. Po naciśnięciu "Zapisz" program zapisuje pliki w folderze gdzie znajdowały się oryginały dodając słowo "resized_" przed każdym z nich.</br>


Program wydany jest na licencji GNU GPL, każdy ma możliwość modyfikowania, prosiłbym jedynie o dodanie informacji o twórcy.</br>

Program był tworzony w ramach nauki programowania w języku Python, a konkretnie pakietu Pillow, Tkinter i PyInstaller, autor nie odpowiada za błędy wynikłe z jego użytkowania.</br>

W razie pytań proszę pisać na: mariusz.jarosz@protonmail.com</br>
Mariusz Jarosz




 
